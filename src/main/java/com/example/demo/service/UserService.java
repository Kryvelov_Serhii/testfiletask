package com.example.demo.service;

import com.example.demo.model.User;
import org.springframework.security.core.userdetails.UserDetailsService;


public interface UserService extends UserDetailsService {

    User save(User user);

    boolean isUserExistsByEmail(String email);

    User findById(Long id);

    User getCurrentUser();

    User findByEmail(String email);

}
