package com.example.demo.service.impl;

import com.example.demo.service.FileStorageService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class FileStorageServiceImpl implements FileStorageService {

    @Value("${com.demo.uploads.directory}")
    private String uploadPath;

    @Override
    public Resource getFile(String filename) {
        Resource resource;
        try {
            Path filePath = Paths.get(uploadPath + filename).toAbsolutePath().normalize();
            resource = new UrlResource(filePath.toUri());
            if (!resource.exists()) {
                return null;
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
        return resource;
    }

    @Override
    public boolean saveFile(String filename, MultipartFile file) {
        try {
            byte[] bytes = file.getBytes();
            Path path = Paths.get(uploadPath + filename);
            Files.write(path, bytes);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}