package com.example.demo.service.impl;

import com.example.demo.model.File;
import com.example.demo.repository.FileRepository;
import com.example.demo.service.FileService;
import org.springframework.stereotype.Service;

@Service
public class FileServiceImpl implements FileService {

    private final FileRepository fileRepository;

    public FileServiceImpl(FileRepository fileRepository) {
        this.fileRepository = fileRepository;
    }

    @Override
    public File findById(String id){
        return fileRepository.findById(id);
    }

    @Override
    public void save(File file){
        fileRepository.save(file);
    }
}
