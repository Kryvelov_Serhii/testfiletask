package com.example.demo.controller;

import com.example.demo.model.File;
import com.example.demo.model.User;
import com.example.demo.model.request.ShareFileRequest;
import com.example.demo.service.FileService;
import com.example.demo.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/api/share")
public class ShareFileController {

    private final UserService userService;
    private final FileService fileService;

    public ShareFileController(UserService userService, FileService fileService) {
        this.userService = userService;
        this.fileService = fileService;
    }

    @PostMapping()
    public ResponseEntity getFiles(@RequestBody @Valid ShareFileRequest request) {
        User user = userService.getCurrentUser();

        User userToShareFile = userService.findByEmail(request.getEmail());
        if (userToShareFile == null)
            return ResponseEntity.badRequest().body("User is not exist");

        if (userToShareFile.getEmail().equals(user.getEmail()))
            return ResponseEntity.badRequest().body("This user is owner");


        File file = fileService.findById(request.getFileId());
        if (file == null)
            return ResponseEntity.badRequest().body("File is not exist");

        if (!file.getUser().equals(user))
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Access denied");

        if (userToShareFile.getSharedFiles().contains(file))
            return ResponseEntity.badRequest().body("File already shared");

        file.getUsersShared().add(userToShareFile);
        userToShareFile.getSharedFiles().add(file);
        userService.save(userToShareFile);

        return ResponseEntity.ok().build();
    }
}
