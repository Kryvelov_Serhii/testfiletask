package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
public class File {

    @Id
    private String id;

    @Column
    private String fileName;

    @ManyToOne()
    @JsonIgnore
    private User user;

    @ManyToMany(mappedBy = "sharedFiles")
    @JsonIgnore
    private List<User> usersShared;

}
