package com.example.demo.model.request;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class ShareFileRequest {

    @NotEmpty
    private String email;

    @NotEmpty
    private String fileId;
}
